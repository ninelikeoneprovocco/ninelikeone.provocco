package one.like.nine.provocco.activities;

import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import one.like.nine.provocco.R;


public class TutorialScreenActivity extends AppIntro {

    public static final String MODE_KEY = "is_passenger_mode";

    @Override
    public void init(Bundle savedInstanceState) {

        boolean isUser = false;
        if (getIntent().hasExtra(MODE_KEY)) {
            isUser = getIntent().getExtras().getBoolean(MODE_KEY, false);
        }

        // userPrefs.mode().get() == User.DRIVER_MODE


        // public static AppIntroFragment newInstance(CharSequence title, CharSequence description, int imageDrawable, int bgColor, int titleColor, int descColor) {

        addSlide(AppIntroFragment.newInstance(
                getString(R.string.tutorial_title_1),
                getString(R.string.tutorial_description_1),
                R.drawable.tuto1,
                R.color.tutorial_bg_color_1,
                R.color.tutorial_title_color_1,
                R.color.tutorial_decr_color_1
        ));

        addSlide(AppIntroFragment.newInstance(
                getString(R.string.tutorial_title_2),
                getString(R.string.tutorial_description_2),
                R.drawable.tuto2,
                R.color.tutorial_bg_color_2,
                R.color.tutorial_title_color_2,
                R.color.tutorial_decr_color_2
        ));

        addSlide(AppIntroFragment.newInstance(
                getString(R.string.tutorial_title_3),
                getString(R.string.tutorial_description_3),
                R.drawable.tuto1,
                R.color.tutorial_bg_color_3,
                R.color.tutorial_title_color_3,
                R.color.tutorial_decr_color_3
        ));

        addSlide(AppIntroFragment.newInstance(
                getString(R.string.tutorial_title_4),
                getString(R.string.tutorial_description_4),
                R.drawable.tuto2,
                R.color.tutorial_bg_color_4,
                R.color.tutorial_title_color_4,
                R.color.tutorial_decr_color_4
        ));

        addSlide(AppIntroFragment.newInstance(
                getString(R.string.tutorial_title_5),
                getString(R.string.tutorial_description_5),
                R.drawable.tuto1,
                R.color.tutorial_bg_color_5,
                R.color.tutorial_title_color_5,
                R.color.tutorial_decr_color_5
        ));



        showSkipButton(false);
        setProgressButtonEnabled(true);
        setSkipText("Passer");
        setDoneText("Fermer");
    }

    @Override
    public void onSkipPressed() {
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed() {
        // Do something when users tap on Done button.
        finish();
    }

    @Override
    public void onSlideChanged() {
        // Do something when the slide changes.
    }

    @Override
    public void onNextPressed() {
        // Do something when users tap on Next button.
    }
}
