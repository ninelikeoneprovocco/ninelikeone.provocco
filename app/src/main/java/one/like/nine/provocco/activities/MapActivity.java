package one.like.nine.provocco.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.hardware.GeomagneticField;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import one.like.nine.provocco.R;

/**
 * Created by IonutH on 02.04.2016.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final int MAP_LOCATION_ZOOM_LEVEL = 13;
    public LatLng currentLocation, clickCurrentLocation, pinLocation;
    public Marker locationMarker, currentLocationMarker;
    ArrayList<one.like.nine.provocco.model.Location> locations;
    private GoogleMap mMap;
    private MarkerOptions options;
    private Float myLat, myLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        init();
    }

    private void init() {
        locations = new ArrayList<>();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setMap();
    }


    /**
     * Set map with the coordinates of your current location
     */
    private void setMap() {
        if (mMap != null) {
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

            // Showing status
            if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available
                int requestCode = 10;
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
                dialog.show();
            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {
                        float bearingTo = location.bearingTo(location);
                        System.out.println("bearingTo= " + bearingTo);
                        GeomagneticField geomagneticField = new GeomagneticField(Double.valueOf(location.getLatitude()).floatValue(),
                                Double.valueOf(location.getLongitude()).floatValue(),
                                Double.valueOf(location.getAltitude()).floatValue(),
                                System.currentTimeMillis());

                        myLat = (float) location.getLatitude();
                        myLng = (float) location.getLongitude();
                        System.out.println("myLat=" + myLat + ",  myLng=" + myLng);

                        if (myLat != null && myLng != null) {
                            locations.add(new one.like.nine.provocco.model.Location(myLat - 1, myLng - 1));
                            locations.add(new one.like.nine.provocco.model.Location(myLat + 1, myLng + 1));
                            locations.add(new one.like.nine.provocco.model.Location(myLat, myLng - 1));
                            locations.add(new one.like.nine.provocco.model.Location(myLat + 1, myLng));
                            addMarkers();
                        }

                        Geocoder gcd = new Geocoder(MapActivity.this, Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = gcd.getFromLocation(myLat, myLng, 1);
                            if (addresses.size() > 0) {
                                String city = addresses.get(0).getLocality();
                                System.out.println("Address=" + addresses.get(0).getLocality());
                                System.out.println(addresses.get(0).toString());


                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentLocation, MAP_LOCATION_ZOOM_LEVEL);
                        mMap.animateCamera(cameraUpdate);

                        options = new MarkerOptions();
                        options.position(currentLocation);
                        options.title(getResources().getString(R.string.my_location));
                        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin));
                        currentLocationMarker = mMap.addMarker(options);
                        currentLocationMarker.showInfoWindow();

                        mMap.setMyLocationEnabled(false);
                        listenerMap();
                    }
                });

            }
        }
    }


    /**
     * Add other pinMarkers
     */
    private void addMarkers() {
        if (locations != null && locations.size() > 0) {
            for (int i = 0; i < locations.size(); i++) {
                pinLocation = new LatLng(locations.get(i).getLat(), locations.get(i).getLng());
                options = new MarkerOptions();
                options.position(pinLocation);
                options.title(pinLocation.toString());
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin));
                mMap.addMarker(options);
            }
        }
    }

    /**
     * Listeners for map
     */
    private void listenerMap() {
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (locationMarker != null)
                    locationMarker.remove();

                clickCurrentLocation = latLng;
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(clickCurrentLocation, MAP_LOCATION_ZOOM_LEVEL);
                mMap.animateCamera(cameraUpdate);

                options = new MarkerOptions();
                options.position(clickCurrentLocation);
                locationMarker = mMap.addMarker(options);
                locationMarker.showInfoWindow();
            }
        });
    }
}