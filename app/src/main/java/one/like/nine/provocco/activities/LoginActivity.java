package one.like.nine.provocco.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import one.like.nine.provocco.R;
import one.like.nine.provocco.application.App;
import one.like.nine.provocco.application.AppUtils;
import one.like.nine.provocco.application.SharedPreferencesSingleton;
import one.like.nine.provocco.model.RegisterResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity implements Callback<RegisterResponse> {

    private LoginButton btnLogin;
    private String fbEmail;
    private String fbFirstName;
    private String fbLastName;
    private Uri fbProfilePicUrl;


    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupView();


    }

    private void setupView() {

        if (AppUtils.isNullOrEmpty(SharedPreferencesSingleton.getProfileId()) == false) {
            Intent login = new Intent(this, MainActivity.class);
            startActivity(login);
            return;

        }

        btnLogin = (LoginButton) findViewById(R.id.btnLoginButton);
        btnLogin.setReadPermissions("user_friends");


        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        btnLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        setFacebookData(loginResult);
                        login();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        sha();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_birthday", "user_friends"));

    }


    private void login() {

        App.getWebServices().registerUserAsync(this, "fbToken", fbProfilePicUrl != null ? fbProfilePicUrl.toString() : "", fbEmail, "location", fbFirstName + " " + fbLastName, 20);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void success(RegisterResponse registerResponse, Response response) {

        Intent login = new Intent(this, MainActivity.class);
        startActivity(login);

        SharedPreferencesSingleton.saveProfileId(registerResponse.getId());
    }

    @Override
    public void failure(RetrofitError error) {

        Toast.makeText(getApplicationContext(), "Failed to login " + error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    private void setFacebookData(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            Log.i("Response", response.toString());

                            fbEmail = response.getJSONObject().getString("email");
                            fbFirstName = response.getJSONObject().getString("first_name");
                            fbLastName = response.getJSONObject().getString("last_name");
                            String gender = response.getJSONObject().getString("gender");
                            String bday = response.getJSONObject().getString("birthday");

                            Profile profile = Profile.getCurrentProfile();
                            String id = profile.getId();
                            String link = profile.getLinkUri().toString();
                            Log.i("Link", link);
                            if (Profile.getCurrentProfile() != null) {
                                Log.i("Login", "ProfilePic= " + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                                fbProfilePicUrl = Profile.getCurrentProfile().getProfilePictureUri(200, 200);
                            }

                            Log.i("Login" + "Email", fbEmail);
                            Log.i("Login" + "FirstName", fbFirstName);
                            Log.i("Login" + "LastName", fbLastName);
                            Log.i("Login" + "Gender", gender);
                            Log.i("Login" + "Bday", bday);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }


    private void sha() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
