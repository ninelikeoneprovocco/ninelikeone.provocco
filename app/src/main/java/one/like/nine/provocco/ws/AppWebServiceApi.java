package one.like.nine.provocco.ws;

import one.like.nine.provocco.model.ApproveChallengeResponse;
import one.like.nine.provocco.model.ChallengeCreateRequest;
import one.like.nine.provocco.model.ChallengeCreateResponse;
import one.like.nine.provocco.model.ChallengeResponse;
import one.like.nine.provocco.model.ChallengersResponse;
import one.like.nine.provocco.model.ChallengesResponse;
import one.like.nine.provocco.model.CompleteChallengeResponse;
import one.like.nine.provocco.model.EditProfileRequest;
import one.like.nine.provocco.model.EditProfileResponse;
import one.like.nine.provocco.model.FileResponse;
import one.like.nine.provocco.model.JoinChallengeResponse;
import one.like.nine.provocco.model.ProfileResponse;
import one.like.nine.provocco.model.RegisterRequest;
import one.like.nine.provocco.model.RegisterResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 *  Service API list
 */
interface AppWebServiceApi {

    @POST("/register")
    void registerUserAsync(@Body RegisterRequest registerRequest, Callback<RegisterResponse> callback);

    @POST("/edit")
    void editUserAsync(@Body EditProfileRequest editRequest, Callback<EditProfileResponse> callback);

    @POST("/create")
    void createChallenge(@Body ChallengeCreateRequest createRequest, Callback<ChallengeCreateResponse> callback);

    @POST("/joinChallenge/{challengeId}/{profileId}")
    void joinChallenge(@Path("challengeId") String challengeId, @Path("profileId") String profileId, Callback<JoinChallengeResponse> callback);

    @Multipart
    @POST("/completeChallenge/{challengeId}/{profileId}")
    void completeChallengeAsync(@Path("challengeId") String challengeId, @Path("profileId") String profileId,
                           @Part("file") TypedFile file,
                           Callback<CompleteChallengeResponse> callback);

    @POST("/aproveChallenger/challengeId}/{profileId}")
    void aproveChallenger(@Path("challengeId") String challengeId, @Path("profileId") String profileId, Callback<ApproveChallengeResponse> callback);

    @GET("/getProfile/{profileId}")
    void registerUserAsync(@Path("profileId") String profileId, Callback<ProfileResponse> callback);

    @GET("/getChallenges")
    void getChallenges( Callback<ChallengesResponse> callback);

    @GET("/getChallenge/{challengeId}")
    void getChallenge(@Path("challengeId") String challengeId, Callback<ChallengeResponse> callback);

    @GET("/getChallengers/{challengeId}")
    void getChallengers(@Path("challengeId") String challengeId, Callback<ChallengersResponse> callback);

    @GET("/getFile/{challengeId}/{profileId}")
    void getFile(@Path("challengeId") String challengeId, @Path("profileId") String profileId, Callback<FileResponse> callback);

}
