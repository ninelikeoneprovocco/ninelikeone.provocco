package one.like.nine.provocco.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by vsimion on 4/2/2016.
 */
public class BaseFragment extends Fragment {
    protected Activity activity;
    protected Bundle savedInstanceState;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.activity = getActivity();
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onCreateFinished();
    }

    protected void onCreateFinished() {
    }
}
