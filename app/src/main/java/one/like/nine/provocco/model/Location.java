package one.like.nine.provocco.model;

import java.io.Serializable;

/**
 * Created by IonutH on 02.04.2016.
 */
public class Location implements Serializable {
    private Float lat, lng;

    public Location() {
    }

    public Location(Float lat, Float lng) {
        setLat(lat);
        setLng(lng);
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }
}
