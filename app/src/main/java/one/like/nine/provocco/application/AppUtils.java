package one.like.nine.provocco.application;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by vmacari on 4/2/16.
 */
public class AppUtils {

    /**
     * Check if {@link String} is null or epmty null or empty.
     *
     * @param val
     * @return
     */
    public static final boolean isNullOrEmpty(String val) {
        return val == null || val.trim().length() == 0;
    }

    public static boolean isNetworkConnected(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if( networkInfo == null ) return false;
        return networkInfo.isConnected();

    }

    public static boolean isWifiConnected(Context cntx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) cntx.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo.isConnected();
    }

    /**
     *      Works well with null strings (two null strings are not equal)
     * @param str1
     * @param str2
     * @return
     */
    public static boolean areStringEqualsIgnoreCase(String str1, String str2) {

        // null strings cannot be qual
        if (str1 == null && str2 == null) {
            return true;
        }

        // null strings cannot be qual
        if (str1 == null || str2 == null) {
            return false;
        }

        return str1.compareToIgnoreCase(str2) == 0;
    }

    public static boolean deleteFile(File storageFolder, String filePath) {
        if (storageFolder == null || !storageFolder.exists() || filePath == null) {
            return false;
        }

        try{
            File fullFilePath = new File(storageFolder, filePath);
            if (fullFilePath == null) {
                return false;
            }

            fullFilePath.delete();
        } catch (Exception e) {

            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean isOnline(Context cntx) {
        ConnectivityManager cm =
                (ConnectivityManager) cntx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static double getScreenDiagonaleInches(){

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager)App.getApp().getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics( dm );
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);

        return screenInches;

    }

    public static void showKeyBoard ( ) {

    }

    public static void hideKeyBoard ( Activity activity ) {
        InputMethodManager imm = (InputMethodManager) App.getApp().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow( activity.getWindow().getDecorView().getWindowToken(), 0 );
    }


    public static String secondsToHumanReadableTime (int timeInSeconds ) {

        StringBuilder sb = new StringBuilder();

        if (timeInSeconds >= 60 ) {
            int minutes = timeInSeconds / 60;
            sb.append(String.format("%02d:", minutes));

            timeInSeconds = timeInSeconds - (minutes * 60);
            sb.append(String.format("%02d", timeInSeconds));
        } else {
            sb.append(String.format("00:%02d", timeInSeconds));

        }
        return sb.toString();
    }


    public static boolean isMobileDataConnected( Context cntx ){
        ConnectivityManager connectivityManager = (ConnectivityManager) cntx.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if( networkInfo == null ) return false;
        return networkInfo.isConnected();
    }


    /**
     *
     * @param latitude
     * @param longitude
     * @return
     */
    public static String getGeocodeAddress (Context cntx, double latitude, double longitude) {

        if(isNetworkConnected( cntx )){
            return "";
        }

        // detect address and send it to server
        Geocoder geocoder = new Geocoder(cntx, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (addresses == null || addresses.size()  == 0) {
            return null;
        }

        // long time = SirApplicationUtils.getLocalToUtcDelta();
        return  String.format(
                "%s, %s, %s",
                // If there's a street address, add it
                addresses.get(0).getMaxAddressLineIndex() > 0 ?
                        addresses.get(0).getAddressLine(0) : "",
                // Locality is usually a city
                addresses.get(0).getLocality(),
                // The country of the address
                addresses.get(0).getCountryName());
    }


    /**
     *
     * @param runnable
     */
    public static Thread runOnSeparatedThread(String tag, Runnable runnable) {
        Thread th = new Thread(runnable, String.format("%s_thread", tag));
        th.start();
        return th;
    }

    /**
     *
     * @param fileName
     * @param context
     * @return
     */
    public static String readFromAssetFile(String fileName, Context context) {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open(fileName);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        return returnString.toString();
    }

    public static void runOnUiThread(Activity activity, Runnable runnable) {
        if (runnable != null && activity != null) {
            activity.runOnUiThread(runnable);
        } else {
            //   SirLogger.e("runOnUiThread :: Cannot execute code, null values passed!");
        }
    }

    public static String map2string(Map<String, Double> probabilities) {
        StringBuilder sb = new StringBuilder();

        for (String k : probabilities.keySet()) {

            if (sb.length() > 0 ) {
                sb.append(", ");
            }
            sb.append(String.format("%s : %f", k, probabilities.get(k)));

        }

        return sb.toString();

    }



    public static String joinStrings (List<String> strings, String separator) {
        StringBuilder sb = new StringBuilder();

        if (strings == null || strings.size() <= 0) {
            return sb.toString();
        }

        for(String s : strings) {
            if (sb.length() > 0) {
                sb.append(separator);
            }

            sb.append(s);
        }

        return sb.toString();
    }

    /**
     *
     * @param enabledCategories
     * @param separator
     * @return
     */
    public static List<String> splitStrings(String enabledCategories, String separator) {
        List<String> result = new ArrayList<String>();

        if (enabledCategories == null || enabledCategories.length() == 0) {
            return result;
        }

        String resultArray [] = enabledCategories.split(separator);

        if (resultArray == null || resultArray.length == 0) {
            return result;
        }

        return Arrays.asList(resultArray);
    }

    public static Date getCurrentGMTDateTime() {

        Date date = Calendar.getInstance().getTime();

        TimeZone tz = TimeZone.getDefault();
        Date ret = new Date( date.getTime() - tz.getRawOffset() );

        // if we are now in DST, back off by the delta.  Note that we are checking the GMT date, this is the KEY.
        if ( tz.inDaylightTime( ret )){
            Date dstDate = new Date( ret.getTime() - tz.getDSTSavings() );

            // check to make sure we have not crossed back into standard time
            // this happens when we are on the cusp of DST (7pm the day before the change for PDT)
            if ( tz.inDaylightTime( dstDate )){
                ret = dstDate;
            }
        }
        return ret;
    }


    /**
     *      The application is executing the runnable on a scheduled period of time within a separated
     *   thread to avoid timer stuck.
     * @param period The periodicity
     * @param runnable The runnable which is detached every period of time
     * @return the timer that is started
     */
    public static Timer startDetachedTimerTask(final String tag, final long period, final Runnable runnable ) {

        final Timer timerTask = new Timer(String.format("%s_timer", tag));
        timerTask.scheduleAtFixedRate(
                new TimerTask() {
                    @Override
                    public void run() {
                        runOnSeparatedThread(tag, runnable);
                    }
                }, 0, period);

        return timerTask;
    }

    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection collection) {
        return !isEmpty(collection);
    }

    public static boolean isEmpty(Map<String, Double> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(Map<String, Double> map) {
        return !isEmpty(map);
    }
}
