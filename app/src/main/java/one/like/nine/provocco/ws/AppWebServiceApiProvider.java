package one.like.nine.provocco.ws;

import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import one.like.nine.provocco.R;
import one.like.nine.provocco.application.App;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.Client;
import retrofit.client.OkClient;

class AppWebServiceApiProvider {

    private static AppWebServiceApi serverApi;

    public static AppWebServiceApi getApi() {
        if (serverApi == null) {
            Client client = createClient();

            JacksonConverter converter = new JacksonConverter();
            AppRequestInterceptor interceptor = new AppRequestInterceptor();

            String endpoint_url = App.getApp().getResources().getString(R.string.web_services_url);

            // TODO disable logging for release - remove setLogLevel(...)
            serverApi = new RestAdapter.Builder().setEndpoint( endpoint_url ).setClient(client)
                    //.setLogLevel(LogLevel.FULL)
                    .setRequestInterceptor(interceptor).setConverter(converter).setLogLevel(LogLevel.FULL)
                    .build().create(AppWebServiceApi.class);
        }

        return serverApi;
    }

    private static Client createClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setHostnameVerifier(createHostnameVerifier());
        okHttpClient.setReadTimeout(10, TimeUnit.MINUTES);

        try {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, createX509TrustManager(), null);
            okHttpClient.setSslSocketFactory(context.getSocketFactory());
        } catch (Exception e) {
        }
        return new OkClient(okHttpClient);
    }

    private static HostnameVerifier createHostnameVerifier() {
        return new HostnameVerifier() {

            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
    }

    private static X509TrustManager[] createX509TrustManager() {
        return new X509TrustManager[] { new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        } };
    }
}
