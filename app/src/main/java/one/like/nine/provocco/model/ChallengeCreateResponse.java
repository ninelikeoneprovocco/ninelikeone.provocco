package one.like.nine.provocco.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class ChallengeCreateResponse extends  GenericChallenge{
    private int id;

    public int getId() {
        return id;
    }
}
