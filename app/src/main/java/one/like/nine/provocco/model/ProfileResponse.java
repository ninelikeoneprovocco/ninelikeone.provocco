package one.like.nine.provocco.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileResponse extends GenericUser{
    private int[] challengesCreated;
    private int[] challengesCompleted;
    private int[] challengesJoined;

    public int[] getChallengesCreated() {
        return challengesCreated;
    }

    public int[] getChallengesCompleted() {
        return challengesCompleted;
    }

    public int[] getChallengesJoined() {
        return challengesJoined;
    }
}
