package one.like.nine.provocco.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.lang.ref.WeakReference;

import one.like.nine.provocco.services.WebService;

public class App extends Application {

    private static WeakReference<App> appContext;
    private WebService webService = new WebService();

    public static Context getAppContext() {
        return appContext.get();
    }

    public static App getApp() {
        return appContext.get();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        appContext = new WeakReference<>(this);
        SharedPreferencesSingleton.initContext(appContext.get());
    }

    /**
     *
     * @return
     */
    public String getApplicationBuildVersion() {

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return pInfo.versionName;
    }

    public static WebService getWebServices() {
        return getApp().webService;
    }

    public void logoutUser() {
        SharedPreferencesSingleton.removeProperty(SharedPreferencesSingleton.Keys.PROFILE_ID);
    }

}