package one.like.nine.provocco.model;

public class CompleteChallengeRequest {
    private String file;

    public String getFile() {
        return this.file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
