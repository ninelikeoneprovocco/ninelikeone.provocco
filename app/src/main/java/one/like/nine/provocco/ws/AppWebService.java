package one.like.nine.provocco.ws;

import android.util.Log;

import java.io.File;

import one.like.nine.provocco.model.CompleteChallengeResponse;
import one.like.nine.provocco.model.EditProfileResponse;
import one.like.nine.provocco.model.RegisterRequest;
import one.like.nine.provocco.model.RegisterResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

public class AppWebService extends GenericWebService {

    private static final String BINARY_MME_TYPE = "application/octet-stream";
    protected static final AppWebService INSTANCE = new AppWebService();

    private AppWebService() {
    }

    public static AppWebService getInstance() {
        return INSTANCE;
    }


    /**
     *      Login webservice interface
     * @param username
     * @param password
     * @param registrationId
     * @param deviceHwId
     * @return
     */

    private String fbToken;
    private String pictureUrl;
    private String email;
    private String location;
    private String name;
    private int age;

    public void registerUserAsync(final Callback<RegisterResponse> callback, final String fbToken, final String pictureUrl,
                                  final String email, String location, final String name, final int age ) {
        try {
            getApi().registerUserAsync(new RegisterRequest( fbToken, pictureUrl, email, location,name,age), callback);
        } catch (Exception e) {
            Log.e(TAG, "Login user error : " + e.getMessage(), e);
            if (callback != null) {
                callback.failure( RetrofitError.unexpectedError("registerUserAsync", e));
            }
        }
    }

    public void editUserAsync(final Callback<EditProfileResponse> callback, final String fbToken, final String pictureUrl,
                              final String email, String location, final String name, final int age ) {
    }

    public void completeChallengeAsync(final Callback<CompleteChallengeResponse> callback,
                                       final String challengeId,final String profileId,
                                       final String filePath) {
        try {
            TypedFile typedFile = new TypedFile("multipart/form-data", new File(filePath));
            getApi().completeChallengeAsync(challengeId, profileId, typedFile,  callback );
        } catch (Exception e) {
            Log.e(TAG, "Complete challenge error : " + e.getMessage(), e);
            if (callback != null) {
                callback.failure( RetrofitError.unexpectedError("completeChallengeAsync", e));
            }
        }
    }

}


