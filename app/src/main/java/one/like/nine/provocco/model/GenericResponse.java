package one.like.nine.provocco.model;

/**
 * Created by mguzun on 4/2/16.
 */
public class GenericResponse {
    protected String message; // server confirmation message


    public GenericResponse () {}
    public GenericResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
