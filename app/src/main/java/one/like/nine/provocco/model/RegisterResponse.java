package one.like.nine.provocco.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterResponse extends GenericUser{
    private boolean newUser;

    public RegisterResponse() {}

    public RegisterResponse(String message) {
        super(message);
    }
}
