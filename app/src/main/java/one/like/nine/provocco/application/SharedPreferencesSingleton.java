package one.like.nine.provocco.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashSet;
import java.util.Set;

public class SharedPreferencesSingleton {

    /**
     * Name of the shared preferences properties file. The real name has beside it an ".xml" ending.
     */
    private static final String APP_SHARED_PREFS_NAME = "one.like.nine.provocco.application";
    private static final int ZERO = 0;
    private static final String EMPTY_STR = "";


    /**
     * Prevent instantiation.
     */
    private SharedPreferencesSingleton() {
    }

    /**
     * Context of application - main activity.
     */
    private static Context context;

    /**
     * The shared preferences of the application.
     */
    private static SharedPreferences sharedPreferences;

    /**
     * The editor of shared preferences. Use it in synchronized methods or in methods synchronize the property itself.
     */
    private static Editor sharedPreferencesEditor;

    /**
     * Constant names of keys. Use this names of keys to read/write to shared preferences.
     * 
     */
    public static final class Keys {

        public static final String FIRST_RUN = "FIRST_RUN";
        public static final String FB_ID = "FB_ID";
        public static final String PROFILE_ID = "PROFILE_ID";


        /**
         * Prevent instantiation.
         */
        private Keys() {
        }
    }

    public static void initContext(Context context) {
        SharedPreferencesSingleton.context = context;
        sharedPreferences = SharedPreferencesSingleton.context.getSharedPreferences(APP_SHARED_PREFS_NAME,
                Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    /**
     * Read a value of a key as a string.
     * 
     * @param key
     * @return value of key as string
     */
    public static final String getPropertyAsString(String key) {
        return sharedPreferences.getString(key, EMPTY_STR);
    }

    /**
     * Save/store a value of a key as a string.
     * 
     * @param key key
     * @param value value as string
     */
    public static synchronized final void savePropertyAsString(String key, String value) {
        sharedPreferencesEditor.putString(key, value);
        sharedPreferencesEditor.apply();
    }


    /**
     * Save/store a value of a key as a string.
     *
     * @param key key
     * @param values value as string
     */
    public static synchronized final void savePropertyAsStringSet(String key, Set<String> values) {
        sharedPreferencesEditor.putStringSet(key, values);
        sharedPreferencesEditor.apply();
    }


    /**
     * Read a value of a key as a string.
     *
     * @param key
     * @return value of key as string
     */
    public static final Set<String> getPropertyAsStringSet(String key) {
        return sharedPreferences.getStringSet(key, new HashSet<String>());
    }



    /**
     * Read a value of a key as a int.
     * 
     * @param key
     * @return value of key as string
     */
    public static final int getPropertyAsInt(String key) {
        return sharedPreferences.getInt(key, ZERO);
    }
  
    /**
     * Save/store a value of a key as a string.
     * 
     * @param key key
     * @param value value as string
     */
    public static synchronized final void savePropertyAsInt(String key, int value) {
        sharedPreferencesEditor.putInt(key, value);
        sharedPreferencesEditor.apply();
    }

    /**
     * Read a value of a key as long.
     * 
     * @param key
     * @return value
     */
    public static final long getPropertyAsLong(String key) {
        return getPropertyAsLong (key, ZERO);
    }
    

    /**
     * Read a value of a key as long with default value.
     * 
     * @param key
     * @return value
     */
    public static final long getPropertyAsLong(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }


    /**
     * Save/store a value of a key as long.
     * 
     * @param key key
     * @param value
     */
    public static synchronized final void savePropertyAsLong(String key, long value) {
        sharedPreferencesEditor.putLong(key, value);
        sharedPreferencesEditor.apply();
    }

    public static synchronized final void savePropertyAsDouble(String key, double value) {
        sharedPreferencesEditor.putString(key, String.valueOf(value));
        sharedPreferencesEditor.apply();
    }

    
    public static final double getPropertyAsDouble(String key) {
        String value = sharedPreferences.getString(key, "0");
        try { 
            return Double.parseDouble(value);
        } catch (Exception ex) {
            
        }
        
        return 0; 
    }

    /**
     * Read a value of a key as a string.
     * 
     * @param key
     * @return value of key as string
     */
    public static final boolean getPropertyAsBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public static final boolean getPropertyAsBoolean(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    /**
     * Save/store a value of a key as a string.
     * 
     * @param key key
     * @param value value as string
     */
    public static synchronized final void savePropertyAsBoolean(String key, boolean value) {
        sharedPreferencesEditor.putBoolean(key, value);
        sharedPreferencesEditor.commit();
        //sharedPreferencesEditor.apply();
    }

    /**
     * Remove property by key.
     * 
     * @param key
     */
    public static synchronized final void removeProperty(String key) {
        sharedPreferencesEditor.remove(key);
        sharedPreferencesEditor.apply();
    }
    

    /**
     *  Save/store a value as set 
     */
    
    public static synchronized final void savePropertyAsSet(String key, Set<String> values ){
        sharedPreferencesEditor.putStringSet( key, values );
        sharedPreferencesEditor.apply();
    }
    
    public static final Set<String> getPropertyAsSet(String key ){
        return sharedPreferences.getStringSet( key , new HashSet<String>() );
    }

    public static void saveProfileId(String profileId) {
        savePropertyAsString(Keys.PROFILE_ID, profileId);
    }

    public static String getProfileId() {
        return getPropertyAsString(Keys.PROFILE_ID);
    }

    public static void saveFacebookId(String facebookId) {
        savePropertyAsString(Keys.FB_ID, facebookId);
    }

    public static String getFacebookId() {
        return getPropertyAsString(Keys.FB_ID);
    }

    public static void setIsFirstRun(boolean isFirstRun) {
        savePropertyAsBoolean(Keys.FIRST_RUN, isFirstRun);
    }

    public static boolean getIsFirstRun() {
        return getPropertyAsBoolean(Keys.FIRST_RUN, true);
    }

}