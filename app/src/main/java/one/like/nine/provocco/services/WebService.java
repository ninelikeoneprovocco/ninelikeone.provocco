package one.like.nine.provocco.services;

import one.like.nine.provocco.model.RegisterResponse;
import one.like.nine.provocco.ws.AppWebService;
import retrofit.Callback;

/**
 * Created by vmacari on 4/2/16.
 */
public class WebService {

    public void registerUserAsync (Callback<RegisterResponse> responseCallBack, final String fbToken, final String pictureUrl,
                                   final String email, String location, final String name, final int age ) {

        AppWebService.getInstance().registerUserAsync(responseCallBack, "fbToken", "pictureUrl" , "email", "location", "name", 20);
    }

}
