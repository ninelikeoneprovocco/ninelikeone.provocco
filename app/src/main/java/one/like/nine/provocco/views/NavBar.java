//package one.like.nine.provocco.views;
//
//import android.annotation.TargetApi;
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.os.Build;
//import android.support.v4.content.ContextCompat;
//import android.util.AttributeSet;
//import android.view.View;
//import android.widget.Button;
//import android.widget.FrameLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.base.view.ButtonWithIconInCenter;
//import com.vuzum.waycount.R;
//
//
//public class NavBar extends RelativeLayout {
//    private static final int IC_BACk = 0;
//    private static final int IC_DASHBOARD = 1;
//    private static final int TXT_LOGOUT = 2;
//    private int DEFAULT_COLOR;
//    private Context context;
//    private Integer iconDrawableLeft, iconDrawableRight, background;
//    private String title;
//    private Callback mCallback;
//    private TextView txt_title;
//    private View lay_header;
//    private Button leftButton, rightButton;
//
//    public NavBar(Context context) {
//        super(context);
//        this.context = context;
//        init();
//    }
//
//    public NavBar(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        this.context = context;
//        parseAttributes(attrs);
//        init();
//    }
//
//    public NavBar(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        this.context = context;
//        parseAttributes(attrs);
//        init();
//    }
//
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public NavBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        this.context = context;
//        parseAttributes(attrs);
//        init();
//    }
//
//    private void parseAttributes(AttributeSet set) {
//        if (set == null) {
//            return;
//        }
//
//        TypedArray array = getContext().obtainStyledAttributes(set, R.styleable.NavigatioBarItem);
//        iconDrawableLeft = array.getInt(R.styleable.NavigatioBarItem_iconDrawableLeft, 0);
//        iconDrawableRight = array.getInt(R.styleable.NavigatioBarItem_iconDrawableRight, 0);
//        title = array.getString(R.styleable.NavigatioBarItem_valueTitle);
//        DEFAULT_COLOR = ContextCompat.getColor(context, R.color.navigation_bar_yelow);
//        background = array.getInteger(R.styleable.NavigatioBarItem_backgroundColor, DEFAULT_COLOR);
//        array.recycle();
//    }
//
//    private void init() {
//        inflate(getContext(), R.layout.page_header, this);
//        txt_title = (TextView) findViewById(com.base.library.R.id.txt_title);
//        lay_header = findViewById(com.base.library.R.id.lay_header);
//        leftButton = (ButtonWithIconInCenter) findViewById(com.base.library.R.id.img_btn_left);
//        rightButton = (ButtonWithIconInCenter) findViewById(com.base.library.R.id.img_btn_right);
//
//        if (background != null)
//            lay_header.setBackgroundColor(background);
//
//        if (this.title != null)
//            txt_title.setText(this.title);
//
//        if (iconDrawableLeft != null)
//            switch (iconDrawableLeft) {
//                case IC_BACk:
//                    leftButton.setIconBitmapResource(R.drawable.ic_action_back);
//                    break;
//                default:
//                    break;
//            }
//
//        if (iconDrawableRight != null)
//            switch (iconDrawableRight) {
//                case IC_DASHBOARD:
//                    rightButton.setIconBitmapResource(R.drawable.ic_dash);
//                    setStyleForText(rightButton);
//                    break;
//                case TXT_LOGOUT:
//                    rightButton.setText(R.string.logout);
//                default:
//                    break;
//            }
//        setListeners();
//    }
//
//    private void setListeners() {
//        if (leftButton != null)
//            leftButton.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mCallback != null)
//                        mCallback.onClickLeftIcon();
//                }
//            });
//
//        if (rightButton != null)
//            rightButton.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mCallback != null)
//                        mCallback.onClickRightIcon();
//                }
//            });
//    }
//
//    public void hideRightIcon() {
//        if (rightButton != null)
//            rightButton.setVisibility(INVISIBLE);
//    }
//
//    public void hideLeftIcon() {
//        if (leftButton != null)
//            leftButton.setVisibility(INVISIBLE);
//    }
//
//    private void setStyleForText(ButtonWithIconInCenter btn) {
//        btn.setTextColor(ContextCompat.getColor(context, R.color.white));
//    }
//
//    public  void setTitle(String title){
//        txt_title.setText(title);
//    }
//
//    public void setTitleGravityLeft() {
//        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) txt_title.getLayoutParams();
//        params.width = FrameLayout.LayoutParams.WRAP_CONTENT;
//        txt_title.setLayoutParams(params);
//
//        FrameLayout.LayoutParams p = (FrameLayout.LayoutParams) rightButton.getLayoutParams();
//        p.width = FrameLayout.LayoutParams.MATCH_PARENT;
//        rightButton.setLayoutParams(p);
//    }
//
//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//    }
//
//    // Set custom callback
//    public void setCallback(Callback callback) {
//        mCallback = callback;
//    }
//
//    // For communicating with the activity
//    public interface Callback {
//        void onClickLeftIcon();
//
//        void onClickRightIcon();
//    }
//}