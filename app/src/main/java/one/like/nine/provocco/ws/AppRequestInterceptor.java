package one.like.nine.provocco.ws;


import one.like.nine.provocco.application.App;
import retrofit.RequestInterceptor;

class AppRequestInterceptor implements RequestInterceptor {

    @Override
    public void intercept(RequestFacade requestFacade) {
        requestFacade.addHeader("appname", "provocco.1like9");
        requestFacade.addHeader("apppfm", "android");
        requestFacade.addHeader("appliver", String.valueOf(App.getApp().getApplicationBuildVersion()));
    }
}
