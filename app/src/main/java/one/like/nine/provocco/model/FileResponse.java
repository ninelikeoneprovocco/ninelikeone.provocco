package one.like.nine.provocco.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

/**
 * Created by mguzun on 4/2/16.
 */
public class FileResponse extends GenericResponse{

    public FileResponse() {
    }
}
