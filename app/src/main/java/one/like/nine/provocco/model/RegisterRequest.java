package one.like.nine.provocco.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class RegisterRequest extends GenericUser{

    public RegisterRequest () {}

    public RegisterRequest(String fbToken, String pictureUrl, String email, String location, String name, int age) {
        setFbToken(fbToken);
        setPictureUrl(pictureUrl);
        setEmail(email);
        setLocation(location);
        setName(name);
        setAge(age);
    }
}
