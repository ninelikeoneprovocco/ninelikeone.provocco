package one.like.nine.provocco.ws;

import retrofit.mime.TypedByteArray;

public class GenericWebService {
    protected static final String TAG = AppWebService.class.getSimpleName();


    protected GenericWebService() {
    }


    protected AppWebServiceApi getApi() {
        return AppWebServiceApiProvider.getApi();
    }


    protected TypedByteArray createTypedByteArray(byte[] rawSoundData) {

        if( rawSoundData == null ){
            return null;
        }

        return new TypedByteArray("application/octet-stream", rawSoundData) {
            @Override
            public String fileName() {
                return "rawSoundDataFileName";
            }
        };
    }


}
