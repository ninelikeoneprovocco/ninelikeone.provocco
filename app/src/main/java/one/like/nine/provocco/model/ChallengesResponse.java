package one.like.nine.provocco.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

/**
 * Created by mguzun on 4/2/16.
 */

public class ChallengesResponse extends GenericResponse{
    private GenericChallenge[] list;

    public ChallengesResponse() {
    }

    public GenericChallenge[] getList() {
        return list;
    }
}
