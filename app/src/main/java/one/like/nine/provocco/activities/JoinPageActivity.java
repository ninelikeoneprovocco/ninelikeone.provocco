package one.like.nine.provocco.activities;

import android.content.Intent;
import android.media.Rating;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;

import java.sql.BatchUpdateException;

import one.like.nine.provocco.R;

public class JoinPageActivity extends AppCompatActivity {

    private Button btn_upload_file;

    private Button btn_complete_challange;

    private RatingBar rb_rate_challenge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setupView();
        setListener();
    }

    private void setupView(){

        btn_upload_file = (Button) findViewById(R.id.btn_upload_file);

        btn_complete_challange = (Button) findViewById(R.id.btn_complete_challange);

        rb_rate_challenge = (RatingBar) findViewById(R.id.rb_rate_challenge);
    }

    private void setListener(){

        btn_upload_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Upload a File");
            }
        });

        btn_complete_challange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clck");
            }
        });

        rb_rate_challenge.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rb_rate_challenge.setRating(rating);
            }
        });
    }


    private void completeChallange() {
        Intent completeChallange = new Intent(this, MainActivity.class);
        startActivity(completeChallange);
    }
}
