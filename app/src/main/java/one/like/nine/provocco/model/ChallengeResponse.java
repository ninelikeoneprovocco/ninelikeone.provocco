package one.like.nine.provocco.model;

public class ChallengeResponse extends  GenericChallenge{
    private String id;

    private int[] peopleJoined;
    private int[] peopleCompleted;
    private int[] peopleAproved;

    public int[] getPeopleJoined() {
        return peopleJoined;
    }

    public int[] getPeopleCompleted() {
        return peopleCompleted;
    }

    public int[] getPeopleAproved() {
        return peopleAproved;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
