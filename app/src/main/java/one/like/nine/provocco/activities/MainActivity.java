package one.like.nine.provocco.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import one.like.nine.provocco.R;
import one.like.nine.provocco.application.SharedPreferencesSingleton;
import one.like.nine.provocco.fragments.MainFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ResideMenu resideMenu;
    private MainActivity mContext;
    private ResideMenuItem itemHome;
    private ResideMenuItem itemProfile;
    private ResideMenuItem itemCreated;
    private ResideMenuItem itemJoined;
    private ResideMenuItem itemCompleted;
    private ResideMenuItem itemLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        setUpMenu();
        if (savedInstanceState == null)
            changeFragment(new MainFragment());
        Window window = mContext.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(mContext, android.R.color.black));
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (SharedPreferencesSingleton.getIsFirstRun()) {
            Intent tutorialScreen = new Intent(this, TutorialScreenActivity.class);
            startActivity(tutorialScreen);
            SharedPreferencesSingleton.setIsFirstRun(false);

        }
    }

    private void setUpMenu() {

        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        resideMenu.setScaleValue(0.6f);


        // create menu items;
        itemHome = new ResideMenuItem(this, R.drawable.ic_back, "Login");
        itemProfile = new ResideMenuItem(this, R.drawable.ic_back, "Map");
        itemCreated = new ResideMenuItem(this, R.drawable.ic_back, "Tutorial");
        itemJoined = new ResideMenuItem(this, R.drawable.ic_back, "JoinPage");
        itemCompleted = new ResideMenuItem(this, R.drawable.ic_back, "Completed");
        itemLogout = new ResideMenuItem(this, R.drawable.ic_back, "Logout");

        itemHome.setOnClickListener(this);
        itemHome.setLayoutParams(new LinearLayout.LayoutParams(600, ViewGroup.LayoutParams.WRAP_CONTENT));
        itemProfile.setOnClickListener(this);
        itemProfile.setLayoutParams(new LinearLayout.LayoutParams(600, ViewGroup.LayoutParams.WRAP_CONTENT));
        itemCreated.setOnClickListener(this);
        itemCreated.setLayoutParams(new LinearLayout.LayoutParams(600, ViewGroup.LayoutParams.WRAP_CONTENT));
        itemJoined.setOnClickListener(this);
        itemJoined.setLayoutParams(new LinearLayout.LayoutParams(600, ViewGroup.LayoutParams.WRAP_CONTENT));
        itemCompleted.setOnClickListener(this);
        itemCompleted.findViewById(com.special.ResideMenu.R.id.tv_title).setLayoutParams(new LinearLayout.LayoutParams(600, ViewGroup.LayoutParams.WRAP_CONTENT));

        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemCreated, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemCompleted, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemJoined, ResideMenu.DIRECTION_LEFT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });

    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {

        }

        @Override
        public void closeMenu() {

        }
    };

    private void changeFragment(Fragment targetFragment) {
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onClick(View view) {
        if (view == itemHome) {
//            changeFragment(new MainFragment());
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        } else if (view == itemProfile) {
//            changeFragment(new ProfilePreviewFragment());
            startActivity(new Intent(MainActivity.this, MapActivity.class));
        } else if (view == itemCreated) {
            startActivity(new Intent(MainActivity.this, TutorialScreenActivity.class));
            // TODO: 4/2/2016
        } else if (view == itemJoined) {
            startActivity(new Intent(MainActivity.this, JoinPageActivity.class));

            // TODO: 4/2/2016
        } else if (view == itemCompleted) {
            // TODO: 4/2/2016
        } else if (view == itemLogout) {
            // TODO: 4/2/2016
        }

        resideMenu.closeMenu();
    }
}
