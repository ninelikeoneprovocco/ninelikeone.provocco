package one.like.nine.provocco.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import one.like.nine.provocco.R;
import one.like.nine.provocco.model.GenericChallenge;

/**
 * Created by laur on 02.04.2016.
 */
public class ChallengesAdapter extends BaseAdapter {

    private final List<GenericChallenge> challenges;
    private final Context context;
    private final LayoutInflater layoutInflater;

    public ChallengesAdapter(Context context, List<GenericChallenge> challenges) {
        this.challenges = challenges;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return challenges.size();
    }

    @Override
    public Object getItem(int position) {
        return challenges.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChallengesHolder challengesHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.activity_challenge_item, parent, false);
            challengesHolder = new ChallengesHolder(convertView);
            convertView.setTag(challengesHolder);
        } else {
            challengesHolder = (ChallengesHolder) convertView.getTag();
        }

        final GenericChallenge challenge = challenges.get(position);
        challengesHolder.txtChallengeDescription.setText(challenge.getDescription());
        challengesHolder.txtChallengeTitle.setText(challenge.getTitle());
        Glide.with(context).load("https://placekitten.com/100/100").into(challengesHolder.ivChallengeImage);

        return convertView;
    }

    public static class ChallengesHolder {
        ImageView ivChallengeImage;
        TextView txtChallengeTitle;
        TextView txtChallengeDescription;

        ChallengesHolder(View cell) {
            ivChallengeImage = (ImageView) cell.findViewById(R.id.iv_challenge_image);
            txtChallengeTitle = (TextView) cell.findViewById(R.id.txt_challenge_title);
            txtChallengeDescription = (TextView) cell.findViewById(R.id.txt_challenge_description);
        }
    }
}
