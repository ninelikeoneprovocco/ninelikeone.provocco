package one.like.nine.provocco.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import one.like.nine.provocco.R;
import one.like.nine.provocco.adapters.ChallengesAdapter;
import one.like.nine.provocco.model.GenericChallenge;

/**
 * Created by laur on 02.04.2016.
 */
public class ZoomedPreviewActivity extends Activity {
    private ListView lv_challenges;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoomed_preview);
        linkUI();
        populateChallengesList();
    }

    private void populateChallengesList() {

        ArrayList<GenericChallenge> challengesList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            GenericChallenge challenge = new GenericChallenge();
            challenge.setTitle("Challenge " + i);
            challenge.setDescription("Description of challenge number " + i);
            challengesList.add(challenge);
        }
        showList(challengesList);
    }

    private void showList(List<GenericChallenge> challenges) {

        lv_challenges.setAdapter(new ChallengesAdapter(this, challenges));
    }

    private void linkUI() {
        lv_challenges = (ListView) findViewById(R.id.lv_challenges);
    }


}
